const express = require('express');
const routes = express.Router();

const authMiddleware = require('./middlewares/auth')

const UserController = require('./controllers/UserController');
const LoginController = require('./controllers/LoginController');

const TurmaController = require('./controllers/TurmaController');

const LeaguersController = require('./controllers/LeaguersController');


routes.post('/login',LoginController.store);

routes.post('/user', UserController.store);
routes.get('/users', authMiddleware ,UserController.index)
routes.get('/user/:userId', authMiddleware ,UserController.show)
routes.put('/user/:userId', authMiddleware ,UserController.update)

routes.post('/turma', authMiddleware ,TurmaController.store);
routes.get('/turmas', authMiddleware ,TurmaController.index);

routes.post('/leaguer', authMiddleware ,LeaguersController.store);
routes.get('/leaguers', authMiddleware ,LeaguersController.index);
routes.get('/leaguer/:leaguerId', authMiddleware ,LeaguersController.show)
routes.put('/leaguer/:leaguerId', authMiddleware ,LeaguersController.update)



module.exports = routes;