const mongoose = require('mongoose')
const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../.env') });


const {MONGO_URL} = process.env
mongoose.connect(
    MONGO_URL,
{   
    useUnifiedTopology: true, 
    useNewUrlParser: true, 
}) 
.then(() => console.log("MongoDB connected")) 
.catch((err) => console.log(err));

module.exports = mongoose;