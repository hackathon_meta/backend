const Leaguer = require('../Models/LeaguersModel');

module.exports = {

    async store(req, res){
        
        const { name, email, fase, tecnologias, idiomas, turma_id, user_id } = req.body;

        try {
            if(!await Leaguer.findOne({email: email})){
                const CreateLeaguer = await Leaguer.create({
                    name,
                    email, 
                    fase, 
                    tecnologias, 
                    idiomas, 
                    turma_id,
                    user_id
                    
                });
                return res.send({CreateLeaguer});

            }else{
                
                return res.status(400).send({error: 'Leaguer already exists'})
            }
        }
        catch (err){
            return res.status(400).send({ error: err.message});
        }
    },

    async index(req, res) {
        try {
            const Leaguers = await Leaguer.find().populate('user_id').populate('turma_id');

            return res.send({ Leaguers })
        } 
        catch (error) {

            return res.status(400).send({ error: 'Error loading'});
            
        }
    },
    async show(req, res) {
        try {
            const leaguerId = await Leaguer.findById(req.params.leaguerId);

            return res.send({ leaguerId })
        } catch (error) {

            return res.status(400).send({ error: 'Error loading'});
            
        }
    },
    async update(req, res) {
        try {
            const EditLeaguer = await Leaguer.findByIdAndUpdate(req.params.leaguerId, req.body,{new: true});

            await EditLeaguer.save();
            return res.send({EditLeaguer})    
        } catch (error) {
            return res.status(400).send({error: 'Error update '});
        }
    },
    
}