
const User = require('../Models/UserModel');

module.exports = {

    async store(req, res){
        const { cpf } = req.body;

        try {
            if(!await User.findOne({cpf})){
                const CreateUser = await User.create(req.body);

                CreateUser.password = undefined;
                
                    return res.send({ CreateUser});
            }else{
                
                return res.status(400).send({error: 'Admin already exists'})
            }
        }
        catch (err){
            return res.status(400).send({ error: 'Registration failed'});
        }
    },

    async index(req, res) {
        try {
            const Users = await User.find();

            return res.send({ Users })
        } 
        catch (error) {

            return res.status(400).send({ error: 'Error loading'});
            
        }
    },

    async show(req, res) {
        try {
            const userId = await User.findById(req.params.userId);

            return res.send({ userId })
        } catch (error) {

            return res.status(400).send({ error: 'Error loading'});
            
        }
    },

    async update(req, res) {
        try {
            const EditUser = await User.findByIdAndUpdate(req.params.userId, req.body);
    
            return res.send(EditUser)    
        } catch (error) {
            return res.status(400).send({error: 'Error update '});
        }
    },
    
}