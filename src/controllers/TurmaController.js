const Turma = require('../Models/TurmaModel');

module.exports = {

    async store(req, res){
        const { user_id } = req.params;
        const { name } = req.body;

        try {
            if(!await Turma.findOne({name})){
                const CreateTurma = await Turma.create(req.body, user_id);
                
                    return res.send({ CreateTurma, user_id });
            }else{
                
                return res.status(400).send({error: 'Turma already exists'})
            }
        }
        catch (err){
            return res.status(400).send({ error: 'Registration failed'});
        }
    },

    async index(req, res) {
        try {
            const Turmas = await Turma.find().populate('user_id');

            return res.send({ Turmas })
        } 
        catch (error) {

            return res.status(400).send({ error: 'Error loading'});
            
        }
    },
    
}