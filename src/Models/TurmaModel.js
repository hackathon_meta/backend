const mongoose = require('../database');

const CreateTurmaSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId, 
        ref:'User',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
});

const Turma = mongoose.model('Turma', CreateTurmaSchema);

module.exports = Turma;