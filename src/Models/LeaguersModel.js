const mongoose = require('../database');

const CreateLeaguersSchema = new mongoose.Schema({
    name: {
        type: String,
       
    },
    email: {
        type: String,
       
    },
    turma_id: {
        type: mongoose.Schema.Types.ObjectId, 
        ref:'Turma',
        
    },
    fase: {
        type: String,
        
    },
    tecnologias: {
        type: [String],
        
    },
    idiomas: {
        type: [String],
    
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId, 
        ref:'User',
        
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
});

const Leaguers = mongoose.model('Leaguers', CreateLeaguersSchema);

module.exports = Leaguers;