<h2 align="center"> 
	🚧  Back-End Projeto FeedLeaguers 🏅 2022 🚧
</h4>


Back-end desenvolvido para armazenar as informações enviadas pelo Front-end da aplicação Projeto FeedLeaguer.


#### Funcionamento do Sistema 1 :
Você deverá desenvolver as seguintes rotas:

- [x] `[POST]/login: Autenticação para ter acesso ao restante das telas e endpoints`

- [x] `[POST]/user: Criar Usuários`

- [x] `[GET]/users : Endpoint utilizado para exibir usuários já criados.`

- [x] `[POST]/turma: Utilizado para criar novas turmas`

- [x] `[GET]/turmas: Utilizado para exibir as listas de turmas já criadas`

- [x] `[POST]/leaguer: Utilizado para criar o cadastro de novos leaguers`

- [x] `[GET]/leaguers: Utilizado para listar todos os leaguers já criados`

### [Documentação do Postman](https://documenter.getpostman.com/view/19297556/Uz5JGv1R#7d1fa942-3090-4f1b-8eb2-7e0aa29df59d)
